(ns graphql-tutorial.movies-resolver
  (:require [graphql-tutorial.movies-repository :as movies.repo]))

(defn movie [_ args _]
  (let [id (:id args)]
    (movies.repo/get-by-id id)))

(defn movies [_context _args _value]
  (movies.repo/get-all))

(defn movies-by-actor [_ _ value]
  (let [id (:id value)]
    (movies.repo/movies-by-casting-id id)))
