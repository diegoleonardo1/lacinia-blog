(ns graphql-tutorial.graphql-server
  (:require [io.pedestal.http :as http]
            [com.walmartlabs.lacinia.pedestal2 :as lp]
            [graphql-tutorial.schema :as movies-schema]))

(def service (lp/default-service
              (movies-schema/movies-schema)
              nil))

(defonce runnable-service (atom nil))

(defn start []
  (println "Starting the GraphQL server")
  (reset! runnable-service (http/create-server service))
  (http/start @runnable-service))

(defn stop []
  (http/stop @runnable-service)
  (reset! runnable-service nil))

#_(start)
#_(stop)
