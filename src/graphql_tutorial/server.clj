(ns graphql-tutorial.server
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.route.definition :refer [defroutes]]))

(defn respond-hello [request]
  {:status 200
   :headers ["content-type" "application/json"]
   :body {:name "foobar"}})

(defroutes routes
  [[["/"
     ["/hello" {:get respond-hello}]]]])

(defn start []
  (-> {::http/routes routes
       ::http/port 3000
       ::http/type :jetty
       ::http/join? false}
      http/create-server
      http/start))

#_(start)
