(ns graphql-tutorial.movies-repository)

(def db-movies
  [{:id 1
    :title "Mr and Ms Smith"
    :synopsis "Any"
    :casting [10 11]
    :released_at "2010-10-10T10:00:00Z"
    :directed_by "Michael Bay"
    :written_by "Fulano"}
   {:id 2
    :title "Twelve Monkeys"
    :synopsis "Any"
    :casting [10]
    :released_at "2010-10-10T10:00:00Z"
    :directed_by "Cicrano"
    :written_by "Fulano"}])

(defn get-by-id [id]
  (-> (filter #(= (:id %) id) db-movies)
      first))

(defn get-all []
  db-movies)

(defn movies-by-casting-id [id]
  (filter (fn [movie]
            (some #(= id %)
                  (:casting movie)))
          db-movies))
