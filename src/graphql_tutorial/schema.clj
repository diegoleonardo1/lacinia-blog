(ns graphql-tutorial.schema
  (:require [clojure.edn :as edn]
            [com.walmartlabs.lacinia.schema :as schema]
            [com.walmartlabs.lacinia.util :refer [attach-resolvers]]
            [graphql-tutorial.actors-resolver :as actors.resolver]
            [graphql-tutorial.movies-resolver :as movies.resolver]))

(defn movies-schema []
  (-> "./resources/schema.edn"
      slurp
      edn/read-string
      (attach-resolvers {:actors actors.resolver/actors
                         :actor actors.resolver/actor
                         :actors_by_movie actors.resolver/actors-by-movie
                         :movies movies.resolver/movies
                         :movie movies.resolver/movie
                         :movies_by_actor movies.resolver/movies-by-actor})
      schema/compile))
