(ns graphql-tutorial.actors-repository)

(def db-actors
  [{:id 10
    :first_name "Brad"
    :last_name "Pitt"
    :age 50}
   {:id 11
    :first_name "Angelina"
    :last_name "Jolie"
    :age 60}])

(defn get-by-id [id]
  (-> (filter #(= (:id %) id) db-actors)
      first))

(defn get-all []
  db-actors)
