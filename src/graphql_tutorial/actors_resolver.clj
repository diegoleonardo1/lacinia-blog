(ns graphql-tutorial.actors-resolver
  (:require [graphql-tutorial.actors-repository :as actor.repo]))

(defn actor [_ args _]
  (let [id (:id args)]
    (actor.repo/get-by-id id)))

(defn actors [_context _args _value]
  (actor.repo/get-all))

(defn actors-by-movie [_ _ value]
  (let [actors (:casting value)]
    (map actor.repo/get-by-id actors)))
